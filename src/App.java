import model.Account;

public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account();
        account1.setId("ACB123");
        account1.setName("Nguyen Van A");
        account1.setBalance(500);
        Account account2 = new Account("ABC456", "Nguyen Van C", 1000);
        System.out.println(account1.toString());

        System.out.println(account2.toString());
        account1.setBalance(account1.credit(2000));
        account2.setBalance(account2.credit(3000));

        System.out.println("Tk sau khi tang");

        System.out.println(account1.toString());

        System.out.println(account2.toString());
        account1.setBalance(account1.debit(1000));
        account2.setBalance(account2.debit(5000));
        System.out.println("Tk sau khi giam");

        System.out.println(account1.toString());

        System.out.println(account2.toString());
        account2.transferTo(account1, 1000);
        System.out.println("Tk sau khi chuyen");

        System.out.println(account1.toString());

        System.out.println(account2.toString());

    }
}
