package model;

import org.w3c.dom.events.MouseEvent;

public class Account {
    private String id;
    private String name;
    private int balance = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Account() {
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int credit(int amount) {

        return this.balance + amount;
    }

    public int debit(int amount) {
        if (amount <= this.balance) {
            this.balance = this.balance - amount;
        } else {
            System.out.println("Amount excced balance");
        }
        return this.balance;
    }

    public void transferTo(Account destinate, int amount) {
        if (amount <= this.balance) {
            destinate.balance = destinate.balance + amount;
            this.balance = debit(amount);
            System.out.println("Chuyen tien thanh cong");
        } else {
            System.out.println("Amount excced balance");
        }

    }

    @Override
    public String toString() {
        return "Account:[id= " + this.id + ", name: " + this.name + ", balance " + this.balance + "]";
    }
}
